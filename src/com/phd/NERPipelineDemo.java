package com.phd;


import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.util.Properties;
import java.util.stream.Collectors;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class NERPipelineDemo {

    public void demo() {

//        try{
//            MaxentTagger file = new MaxentTagger("./english-left3words-distsim.tagger");
//            System.out.println("File OK");
//        } catch(Exception e) {
//            System.out.println("File ERROR");
//        }

        // set up pipeline properties
        Properties props = new Properties();
//        props.put("pos.model", "./english-left3words-distsim.tagger");
//        props.put("ner.model", "./english.all.3class.distsim.crf.ser.gz");

        //Phd
        //props.put("ner.model", "/Users/svitlanamoiseyenko/OUR/STUDY/PhD/CODING/ML/phd/phd-features-ner-model.ser.gz");

        //Pitch
        props.put("ner.model", "/Users/svitlanamoiseyenko/OUR/STUDY/PhD/CODING/ML/pitch/pitch-ner-model.ser.gz");
        props.setProperty("annotators", "tokenize,ssplit,pos, lemma,ner"); //("annotators", "tokenize,ssplit,pos,lemma,ner")

        // example customizations (these are commented out but you can uncomment them to see the results

        // disable fine grained ner
        //props.setProperty("ner.applyFineGrained", "false");

        // customize fine grained ner
        //props.setProperty("ner.fine.regexner.mapping", "example.rules");
        //props.setProperty("ner.fine.regexner.ignorecase", "true");

        // add additional rules
        //props.setProperty("ner.additional.regexner.mapping", "example.rules");
        //props.setProperty("ner.additional.regexner.ignorecase", "true");

        // add 2 additional rules files ; set the first one to be case-insensitive
        //props.setProperty("ner.additional.regexner.mapping", "ignorecase=true,example_one.rules;example_two.rules");


        // set up pipeline

        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        // make an example document
        //CoreDocument doc = new CoreDocument("Joe Smith is from Seattle.");
        //CoreDocument doc = new CoreDocument("temporal reasoning; temporal reasoning algorithm; temporal world; linear time; time complexity;");
        //CoreDocument doc = new CoreDocument("temporal reasoning \n temporal reasoning algorithm  \n temporal world \n  linear time  \n time complexity");

        CoreDocument doc = new CoreDocument("Looking to raise $10,000,000 to create a Bitcoin Cash mining facility in the middle of the Atlantic Ocean. It will obtain power from a combination of wave power, solar power, and also a local whale gym that will be built as part of this project. Whales will be fitted with secure harnesses and “encouraged” to swim as fast as they can, while dragging a giant turbine, which we dub an “underwater sail”. Most whales survive around 6 months under these conditions, so there is an ongoing need to capture more whales that can be harnessed. There is also a need to connect to low latency internet providers, of which there is a dearth in the middle of the ocean. We theorize that this problem can be easily solved if we only had $10MM.");
        //CoreDocument doc = new CoreDocument("A company that makes perpetual motion machines for free energy.");
        // annotate the document
        pipeline.annotate(doc);
        // view results

        System.out.println("---");
        System.out.println("entities found");
         for (CoreEntityMention em : doc.entityMentions()) //TODO: can extend by our own class, because we already have entities
            System.out.println("\tdetected entity: \t"+em.text()+"\t"+em.entityType());
        System.out.println("---");
        System.out.println("tokens and ner tags");
        String tokensAndNERTags = doc.tokens().stream().map(token -> "("+token.word()+","+token.ner()+")").collect(
                Collectors.joining(" "));
        System.out.println(tokensAndNERTags);

    }

}

/*
Properties props = new Properties();
String modPath = "<YOUR PATH TO MODELS>/models3.4/edu/stanford/nlp/models/";
props.put("pos.model", modPath + "pos-tagger/english-left3words/english-left3words-distsim.tagger");
props.put("ner.model", modPath + "ner/english.all.3class.distsim.crf.ser.gz");
props.put("parse.model", modPath + "lexparser/englishPCFG.ser.gz");
props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
props.put("sutime.binders","0");
props.put("sutime.rules", modPath + "sutime/defs.sutime.txt, " + modPath + "sutime/english.sutime.txt");
props.put("dcoref.demonym", modPath + "dcoref/demonyms.txt");
props.put("dcoref.states", modPath + "dcoref/state-abbreviations.txt");
props.put("dcoref.animate", modPath + "dcoref/animate.unigrams.txt");
props.put("dcoref.inanimate", modPath + "dcoref/inanimate.unigrams.txt");
props.put("dcoref.big.gender.number", modPath + "dcoref/gender.data.gz");
props.put("dcoref.countries", modPath + "dcoref/countries");
props.put("dcoref.states.provinces", modPath + "dcoref/statesandprovinces");
props.put("dcoref.singleton.model", modPath + "dcoref/singleton.predictor.ser");
 */
